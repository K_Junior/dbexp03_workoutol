package cn.dbers.workoutol;

import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;

import cn.dbers.workoutol.mapper.CustomerMapper;
import cn.dbers.workoutol.mapper.CustomerTrainingInfoMapper;
import lombok.ToString;

@SpringBootTest
class WorkoutolApplicationTests {

  @Autowired
  private CustomerMapper customerMapper;

  @Autowired
  private CustomerTrainingInfoMapper cTrainingInfoMapper;

	@Test
	void contextLoads() {
	}

  @Test
  void test(){
      customerMapper.selectList(null).forEach(System.out::println);   
  }

  @Test
  void Votest(){
      cTrainingInfoMapper.listInfoVos().forEach(System.out::println);
  }
}
