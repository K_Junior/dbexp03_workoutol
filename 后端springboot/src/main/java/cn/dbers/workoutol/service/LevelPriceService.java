package cn.dbers.workoutol.service;

import cn.dbers.workoutol.entity.LevelPrice;
import com.baomidou.mybatisplus.extension.service.IService;

/**
 * <p>
 * 训练级别收费表 服务类
 * </p>
 *
 * @author Junior
 * @since 2021-11-26
 */
public interface LevelPriceService extends IService<LevelPrice> {

}
