package cn.dbers.workoutol.service;

import cn.dbers.workoutol.entity.CustomerTrainingInfo;
import cn.dbers.workoutol.vo.CustomerTrainingInfoVo;

import java.util.List;

import com.baomidou.mybatisplus.extension.service.IService;

/**
 * <p>
 * 客户训练情况表 服务类
 * </p>
 *
 * @author Junior
 * @since 2021-11-26
 */
public interface CustomerTrainingInfoService extends IService<CustomerTrainingInfo> {

      List<CustomerTrainingInfoVo> listVos();

}
