package cn.dbers.workoutol.service.impl;

import cn.dbers.workoutol.entity.LevelPrice;
import cn.dbers.workoutol.mapper.LevelPriceMapper;
import cn.dbers.workoutol.service.LevelPriceService;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.springframework.stereotype.Service;

/**
 * <p>
 * 训练级别收费表 服务实现类
 * </p>
 *
 * @author Junior
 * @since 2021-11-26
 */
@Service
public class LevelPriceServiceImpl extends ServiceImpl<LevelPriceMapper, LevelPrice> implements LevelPriceService {

}
