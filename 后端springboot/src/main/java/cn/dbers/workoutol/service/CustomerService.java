package cn.dbers.workoutol.service;

import cn.dbers.workoutol.entity.Customer;
import com.baomidou.mybatisplus.extension.service.IService;

/**
 * <p>
 * 客户信息表 服务类
 * </p>
 *
 * @author Junior
 * @since 2021-11-26
 */
public interface CustomerService extends IService<Customer> {

}
