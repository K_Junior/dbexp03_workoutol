package cn.dbers.workoutol.service.impl;

import cn.dbers.workoutol.entity.CustomerTrainingInfo;
import cn.dbers.workoutol.mapper.CustomerTrainingInfoMapper;
import cn.dbers.workoutol.service.CustomerTrainingInfoService;
import cn.dbers.workoutol.vo.CustomerTrainingInfoVo;

import java.util.List;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

/**
 * <p>
 * 客户训练情况表 服务实现类
 * </p>
 *
 * @author Junior
 * @since 2021-11-26
 */
@Service
public class CustomerTrainingInfoServiceImpl extends ServiceImpl<CustomerTrainingInfoMapper, CustomerTrainingInfo> implements CustomerTrainingInfoService {


  @Autowired
  private CustomerTrainingInfoMapper cInfoMapper;

  @Override
  public List<CustomerTrainingInfoVo> listVos() {
    
    return cInfoMapper.listInfoVos();
  }

    
}
