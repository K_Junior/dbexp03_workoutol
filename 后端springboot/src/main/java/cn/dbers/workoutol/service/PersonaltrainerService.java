package cn.dbers.workoutol.service;

import cn.dbers.workoutol.entity.Personaltrainer;
import com.baomidou.mybatisplus.extension.service.IService;

/**
 * <p>
 * 教练信息表 服务类
 * </p>
 *
 * @author Junior
 * @since 2021-11-26
 */
public interface PersonaltrainerService extends IService<Personaltrainer> {

}
