package cn.dbers.workoutol.service.impl;

import cn.dbers.workoutol.entity.Personaltrainer;
import cn.dbers.workoutol.mapper.PersonaltrainerMapper;
import cn.dbers.workoutol.service.PersonaltrainerService;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.springframework.stereotype.Service;

/**
 * <p>
 * 教练信息表 服务实现类
 * </p>
 *
 * @author Junior
 * @since 2021-11-26
 */
@Service
public class PersonaltrainerServiceImpl extends ServiceImpl<PersonaltrainerMapper, Personaltrainer> implements PersonaltrainerService {

}
