package cn.dbers.workoutol.service.impl;

import cn.dbers.workoutol.entity.Customer;
import cn.dbers.workoutol.mapper.CustomerMapper;
import cn.dbers.workoutol.service.CustomerService;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.springframework.stereotype.Service;

/**
 * <p>
 * 客户信息表 服务实现类
 * </p>
 *
 * @author Junior
 * @since 2021-11-26
 */
@Service
public class CustomerServiceImpl extends ServiceImpl<CustomerMapper, Customer> implements CustomerService {

}
