package cn.dbers.workoutol.controller;


import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import ch.qos.logback.core.pattern.color.BoldBlueCompositeConverter;
import cn.dbers.workoutol.entity.Customer;
import cn.dbers.workoutol.service.CustomerService;

import java.util.List;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;

/**
 * <p>
 * 客户信息表 前端控制器
 * </p>
 *
 * @author Junior
 * @since 2021-11-26
 */
@CrossOrigin
@RestController
@RequestMapping("/customer")
public class CustomerController {

    @Autowired
    private CustomerService customerService;

    @GetMapping("/list")
    public List<Customer> getAll(){
      return customerService.list();
    }

    /**
     * 按页码查看客户信息
     * @param currentPage
     * @param pageSize
     * @return
     */
    @GetMapping("/page/{currentPage}/{pageSize}")
    public Page<Customer> getByPage(@PathVariable int currentPage, @PathVariable int pageSize){
      
      Page<Customer> page = new Page<>(currentPage, pageSize);
      QueryWrapper<Customer> queryWrapper = new QueryWrapper<>();
      queryWrapper.orderByAsc("customer_id");
      return customerService.page(page, queryWrapper);
    }

    /**
     * 根据客户id删除客户信息
     * @param id
     * @return
     */
    @DeleteMapping("/delete/{id}")
    public boolean delete(@PathVariable Integer id){

        return customerService.removeById(id);
    }

    /**
     * 根据id查找客户信息
     * @param id
     * @return
     */
    @GetMapping("/find/{id}")
    public Customer find(@PathVariable Integer id){
        return customerService.getById(id);
    }

    /**
     * 添加用户信息
     * @param customer
     * @return
     */
    @PostMapping("/add")
    public boolean add(@RequestBody Customer customer){
      return customerService.save(customer);
    }

    /**
     * 修改用户信息
     * @param customer
     * @return
     */
    @PutMapping("/update")
    public boolean update(@RequestBody Customer customer){
      return customerService.updateById(customer);
    }

}

