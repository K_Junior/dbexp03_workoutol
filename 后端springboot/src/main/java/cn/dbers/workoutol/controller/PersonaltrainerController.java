package cn.dbers.workoutol.controller;


import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import cn.dbers.workoutol.entity.Personaltrainer;
import cn.dbers.workoutol.service.PersonaltrainerService;

import javax.annotation.PostConstruct;

import com.baomidou.mybatisplus.core.conditions.query.Query;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PostMapping;


/**
 * <p>
 * 教练信息表 前端控制器
 * </p>
 *
 * @author Junior
 * @since 2021-11-26
 */
@CrossOrigin
@RestController
@RequestMapping("/personaltrainer")
public class PersonaltrainerController {

    @Autowired
    private PersonaltrainerService pService;

    @GetMapping("/page/{currentPage}/{pageSize}")
    public Page<Personaltrainer> getByPage(@PathVariable int currentPage, @PathVariable int pageSize)
    {
      Page<Personaltrainer> page = new Page<>(currentPage, pageSize);
      QueryWrapper<Personaltrainer> queryWrapper = new QueryWrapper<>();
      queryWrapper.orderByAsc("personaltrainer_id");
      return pService.page(page, queryWrapper);
    }

    @GetMapping("/find/{id}")
    public Personaltrainer find(@PathVariable int id)
    {
      return pService.getById(id);
    }

    @DeleteMapping("/delete/{id}")
    public boolean delete(@PathVariable int id){
      return pService.removeById(id);
    }

    @PutMapping("/update")
    public boolean update(@RequestBody Personaltrainer personaltrainer)
    {
      return pService.updateById(personaltrainer);
    }

    @PostMapping("/add")
    public boolean add(@RequestBody Personaltrainer personaltrainer) {
      return pService.save(personaltrainer);
    }
    


}

