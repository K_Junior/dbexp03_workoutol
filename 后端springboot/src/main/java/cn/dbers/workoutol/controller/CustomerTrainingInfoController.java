package cn.dbers.workoutol.controller;


import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;

import cn.dbers.workoutol.entity.CustomerTrainingInfo;
import cn.dbers.workoutol.service.CustomerTrainingInfoService;

import java.util.List;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;



/**
 * <p>
 * 客户训练情况表 前端控制器
 * </p>
 *
 * @author Junior
 * @since 2021-11-26
 */
@CrossOrigin
@RestController
@RequestMapping("/customerTrainingInfo")
public class CustomerTrainingInfoController {

  @Autowired
  private CustomerTrainingInfoService cTrainingInfoService;
  
  @GetMapping("/list")
  public List<CustomerTrainingInfo> getAll() {
      return cTrainingInfoService.list();
  }
  

  @GetMapping("/page/{currentPage}/{pageSize}")
  public Page<CustomerTrainingInfo> getByPage(@PathVariable int currentPage, @PathVariable int pageSize){

    Page<CustomerTrainingInfo> page = new Page<>(currentPage, pageSize);
    QueryWrapper<CustomerTrainingInfo> queryWrapper = new QueryWrapper<>();
    queryWrapper.orderByAsc("customer_id");
    return cTrainingInfoService.page(page, queryWrapper);
  }

  @PostMapping(value="/add")
  public boolean add(@RequestBody CustomerTrainingInfo cTrainingInfo) {
      
      return cTrainingInfoService.save(cTrainingInfo);
  }

  @GetMapping("/find/{id}")
  public CustomerTrainingInfo find(@PathVariable int id){

    return cTrainingInfoService.getById(id);
  }

  @PutMapping("/update")
  public boolean update(@RequestBody CustomerTrainingInfo cTrainingInfo)
  {
    return cTrainingInfoService.updateById(cTrainingInfo);
  }

  @DeleteMapping("/delete/{id}")
  public boolean delete(@PathVariable int id)
  {
    return cTrainingInfoService.removeById(id);
  }
  
}

