package cn.dbers.workoutol.controller;


import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import cn.dbers.workoutol.entity.LevelPrice;
import cn.dbers.workoutol.service.LevelPriceService;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestParam;


/**
 * <p>
 * 训练级别收费表 前端控制器
 * </p>
 *
 * @author Junior
 * @since 2021-11-26
 */
@CrossOrigin
@RestController
@RequestMapping("/levelPrice")
public class LevelPriceController {

    @Autowired
    private LevelPriceService levelPriceService;


    @PostMapping("/add")
    public boolean add(@RequestBody LevelPrice levelPrice)
    {
      return levelPriceService.save(levelPrice);
    }

    @GetMapping("/list")
    public List<LevelPrice> getAll(){
      return levelPriceService.list();
    }

    @GetMapping("/find/{id}")
    public LevelPrice find(@PathVariable String id) {
        return levelPriceService.getById(id);
    }
    
    @PutMapping("/update")
    public boolean update(@RequestBody LevelPrice levelPrice)
    {
      return levelPriceService.updateById(levelPrice);
    }

    @DeleteMapping("/delete/{id}")
    public boolean delete(@PathVariable String id){
      return levelPriceService.removeById(id);
    }

}

