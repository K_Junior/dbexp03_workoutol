package cn.dbers.workoutol.mapper;

import cn.dbers.workoutol.entity.LevelPrice;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;

/**
 * <p>
 * 训练级别收费表 Mapper 接口
 * </p>
 *
 * @author Junior
 * @since 2021-11-26
 */
public interface LevelPriceMapper extends BaseMapper<LevelPrice> {

}
