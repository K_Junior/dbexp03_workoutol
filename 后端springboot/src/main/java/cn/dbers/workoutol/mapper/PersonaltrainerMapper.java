package cn.dbers.workoutol.mapper;

import cn.dbers.workoutol.entity.Personaltrainer;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;

/**
 * <p>
 * 教练信息表 Mapper 接口
 * </p>
 *
 * @author Junior
 * @since 2021-11-26
 */
public interface PersonaltrainerMapper extends BaseMapper<Personaltrainer> {

}
