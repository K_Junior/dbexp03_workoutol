package cn.dbers.workoutol.mapper;

import cn.dbers.workoutol.entity.CustomerTrainingInfo;
import cn.dbers.workoutol.vo.CustomerTrainingInfoVo;

import java.util.List;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;

import org.apache.ibatis.annotations.Select;

/**
 * <p>
 * 客户训练情况表 Mapper 接口
 * </p>
 *
 * @author Junior
 * @since 2021-11-26
 */
public interface CustomerTrainingInfoMapper extends BaseMapper<CustomerTrainingInfo> {

    @Select("SELECT cti.customer_id, c.firstname, cti.personaltrainer_id,  pt.pt_firstname, cti.level "+
     "FROM customer c, personaltrainer pt, customer_training_info cti "+
     "WHERE c.customer_id = cti.customer_id and pt.personaltrainer_id = cti.personaltrainer_id")
    public List<CustomerTrainingInfoVo> listInfoVos();

}
