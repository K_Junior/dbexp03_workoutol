package cn.dbers.workoutol.mapper;

import cn.dbers.workoutol.entity.Customer;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;

/**
 * <p>
 * 客户信息表 Mapper 接口
 * </p>
 *
 * @author Junior
 * @since 2021-11-26
 */
public interface CustomerMapper extends BaseMapper<Customer> {

}
