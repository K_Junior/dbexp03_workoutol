package cn.dbers.workoutol.vo;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class CustomerTrainingInfoVo {
  
    private Integer customerId;
    private String firstname;
    private Integer personaltrainerId;
    private String ptFirstname;
    private String level;

}
