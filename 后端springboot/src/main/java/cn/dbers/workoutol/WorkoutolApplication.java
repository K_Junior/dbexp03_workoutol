package cn.dbers.workoutol;

import org.mybatis.spring.annotation.MapperScan;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
@MapperScan("cn.dbers.workoutol.mapper")
public class WorkoutolApplication {

	public static void main(String[] args) {
		SpringApplication.run(WorkoutolApplication.class, args);
	}

}
