package cn.dbers.workoutol.entity;

import com.baomidou.mybatisplus.annotation.TableName;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableField;
import java.io.Serializable;
import lombok.Data;
import lombok.EqualsAndHashCode;

/**
 * <p>
 * 客户训练情况表
 * </p>
 *
 * @author Junior
 * @since 2021-11-26
 */
@Data
  @EqualsAndHashCode(callSuper = false)
    @TableName("customer_training_info")
public class CustomerTrainingInfo implements Serializable {

    private static final long serialVersionUID=1L;

      /**
     * 客户ID
     */
        @TableId("customer_id")
      private Integer customerId;

      /**
     * 教练ID
     */
      @TableField("personaltrainer_id")
    private Integer personaltrainerId;

      /**
     * 训练套餐级别
     */
      @TableField("level")
    private String level;


}
