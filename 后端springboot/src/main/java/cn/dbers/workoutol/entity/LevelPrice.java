package cn.dbers.workoutol.entity;

import java.math.BigDecimal;
import com.baomidou.mybatisplus.annotation.TableName;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableField;
import java.io.Serializable;
import lombok.Data;
import lombok.EqualsAndHashCode;

/**
 * <p>
 * 训练级别收费表
 * </p>
 *
 * @author Junior
 * @since 2021-11-26
 */
@Data
  @EqualsAndHashCode(callSuper = false)
    @TableName("level_price")
public class LevelPrice implements Serializable {

    private static final long serialVersionUID=1L;

      /**
     * 训练套餐级别
     */
        @TableId("level")
      private String level;

      /**
     * 套餐月费用
     */
      @TableField("price_permonth")
    private BigDecimal pricePermonth;


}
