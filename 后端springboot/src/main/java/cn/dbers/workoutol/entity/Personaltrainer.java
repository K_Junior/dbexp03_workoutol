package cn.dbers.workoutol.entity;

import com.baomidou.mybatisplus.annotation.TableName;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableField;
import java.io.Serializable;
import lombok.Data;
import lombok.EqualsAndHashCode;

/**
 * <p>
 * 教练信息表
 * </p>
 *
 * @author Junior
 * @since 2021-11-26
 */
@Data
  @EqualsAndHashCode(callSuper = false)
    @TableName("personaltrainer")
public class Personaltrainer implements Serializable {

    private static final long serialVersionUID=1L;

      /**
     * 教练ID
     */
        @TableId(value="personaltrainer_id",type = IdType.AUTO)
      private Integer personaltrainerId;

      /**
     * 姓
     */
      @TableField("pt_lastname")
    private String ptLastname;

      /**
     * 名
     */
      @TableField("pt_firstname")
    private String ptFirstname;

      /**
     * 地址
     */
      @TableField("pt_address")
    private String ptAddress;

      /**
     * 所在城市
     */
      @TableField("pt_city")
    private String ptCity;

      /**
     * 所在州
     */
      @TableField("pt_state")
    private String ptState;

      /**
     * 邮编
     */
      @TableField("pt_zip")
    private String ptZip;

      /**
     * 电话号码
     */
      @TableField("pt_phone")
    private String ptPhone;

      /**
     * 邮箱地址
     */
      @TableField("pt_email")
    private String ptEmail;


}
