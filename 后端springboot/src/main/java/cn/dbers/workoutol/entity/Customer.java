package cn.dbers.workoutol.entity;

import com.baomidou.mybatisplus.annotation.TableName;
import java.time.LocalDate;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableField;
import java.io.Serializable;
import lombok.Data;
import lombok.EqualsAndHashCode;

/**
 * <p>
 * 客户信息表
 * </p>
 *
 * @author Junior
 * @since 2021-11-26
 */
@Data
  @EqualsAndHashCode(callSuper = false)
    @TableName("customer")
public class Customer implements Serializable {

    private static final long serialVersionUID=1L;

      /**
     * 客户ID
     */
        @TableId(value="customer_id",type = IdType.AUTO)
      private Integer customerId;

      /**
     * 姓
     */
      @TableField("lastname")
    private String lastname;

      /**
     * 名
     */
      @TableField("firstname")
    private String firstname;

      /**
     * 地址
     */
      @TableField("address")
    private String address;

      /**
     * 所在城市
     */
      @TableField("city")
    private String city;

      /**
     * 所在州
     */
      @TableField("state")
    private String state;

      /**
     * 邮编
     */
      @TableField("zip")
    private String zip;

      /**
     * 电话号码
     */
      @TableField("phone")
    private String phone;

      /**
     * 邮箱地址
     */
      @TableField("email")
    private String email;

      /**
     * 信用卡后四位
     */
      @TableField("creditcard_details")
    private String creditcardDetails;

      /**
     * 出生日期
     */
      @TableField("birthdate")
    private LocalDate birthdate;

      /**
     * 健身史
     */
      @TableField("exercise_history")
    private String exerciseHistory;

      /**
     * 身体水平
     */
      @TableField("fitness_level")
    private String fitnessLevel;

      /**
     * 健康状况
     */
      @TableField("medical_heath")
    private String medicalHeath;


}
