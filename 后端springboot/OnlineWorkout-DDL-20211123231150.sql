DROP TABLE IF EXISTS Customer;
CREATE TABLE Customer(
    CustomerID INTEGER NOT NULL AUTO_INCREMENT,
    LastName VARCHAR(30),
    FirstName VARCHAR(30),
    Address VARCHAR(50),
    City VARCHAR(30),
    State VARCHAR(2),
    Zip VARCHAR(5),
    Phone VARCHAR(15),
    EmailAddress VARCHAR(50),
    CreditCardDetails VARCHAR(4),
    BirthDate DATE,
    ExerciseHistory VARCHAR(30),
    FitnessLevel VARCHAR(30),
    MedicalHeath VARCHAR(30),
    PRIMARY KEY (CustomerID)
);

COMMENT ON TABLE Customer IS '客户信息表';
COMMENT ON COLUMN Customer.CustomerID IS '客户ID';
COMMENT ON COLUMN Customer.LastName IS '姓';
COMMENT ON COLUMN Customer.FirstName IS '名';
COMMENT ON COLUMN Customer.Address IS '地址';
COMMENT ON COLUMN Customer.City IS '所在城市';
COMMENT ON COLUMN Customer.State IS '所在州';
COMMENT ON COLUMN Customer.Zip IS '邮编';
COMMENT ON COLUMN Customer.Phone IS '电话号码';
COMMENT ON COLUMN Customer.EmailAddress IS '邮箱地址';
COMMENT ON COLUMN Customer.CreditCardDetails IS '信用卡后四位';
COMMENT ON COLUMN Customer.BirthDate IS '出生日期';
COMMENT ON COLUMN Customer.ExerciseHistory IS '健身史';
COMMENT ON COLUMN Customer.FitnessLevel IS '身体水平';
COMMENT ON COLUMN Customer.MedicalHeath IS '健康状况';

DROP TABLE IF EXISTS Trainer;
CREATE TABLE Trainer(
    PersonalTrainerID INTEGER NOT NULL AUTO_INCREMENT,
    PTLastName VARCHAR(30),
    PTFirstName VARCHAR(30),
    PTAddress VARCHAR(50),
    PTCity VARCHAR(30),
    PTState VARCHAR(2),
    PTZip VARCHAR(5),
    PTPhone VARCHAR(15),
    PTEmailAddress VARCHAR(50),
    PRIMARY KEY (PersonalTrainerID)
);

COMMENT ON TABLE Trainer IS '教练信息表';
COMMENT ON COLUMN Trainer.PersonalTrainerID IS '教练ID';
COMMENT ON COLUMN Trainer.PTLastName IS '姓';
COMMENT ON COLUMN Trainer.PTFirstName IS '名';
COMMENT ON COLUMN Trainer.PTAddress IS '地址';
COMMENT ON COLUMN Trainer.PTCity IS '所在城市';
COMMENT ON COLUMN Trainer.PTState IS '所在州';
COMMENT ON COLUMN Trainer.PTZip IS '邮编';
COMMENT ON COLUMN Trainer.PTPhone IS '电话号码';
COMMENT ON COLUMN Trainer.PTEmailAddress IS '邮箱地址';

DROP TABLE IF EXISTS CustomerTrainingInfo;
CREATE TABLE CustomerTrainingInfo(
    CustomerID INTEGER NOT NULL,
    PersonalTrainerID INTEGER,
    Level VARCHAR(6),
    PRIMARY KEY (CustomerID)
);

COMMENT ON TABLE CustomerTrainingInfo IS '客户训练情况表';
COMMENT ON COLUMN CustomerTrainingInfo.CustomerID IS '客户ID';
COMMENT ON COLUMN CustomerTrainingInfo.PersonalTrainerID IS '教练ID';
COMMENT ON COLUMN CustomerTrainingInfo.Level IS '训练套餐级别';

DROP TABLE IF EXISTS LevelPrice;
CREATE TABLE LevelPrice(
    Level VARCHAR(6) NOT NULL,
    PricePerMonth NUMERIC(4,2),
    PRIMARY KEY (Level)
);

COMMENT ON TABLE LevelPrice IS '训练级别收费表';
COMMENT ON COLUMN LevelPrice.Level IS '训练套餐级别';
COMMENT ON COLUMN LevelPrice.PricePerMonth IS '套餐月费用';

