import Vue from 'vue'
import VueRouter from 'vue-router'
import Home from '../views/Home.vue'
import App from '../App.vue'
import { Table } from 'element-ui'
import Customer from '../components/Customer.vue'
import Personaltrainer from '../components/Personaltrainer.vue'
import CustomerTrainingInfo from '../components/CustomerTrainingInfo.vue'
import LevelPrice from '../components/LevelPrice.vue'

// import AddCustomer from '../components/AddCustomer.vue'

Vue.use(VueRouter)

const routes = [
  // {
  //   path: '/',
  //   name: 'app',
  //   component: App
  // },
  {
    path: '/',
    name: '导航',
    component: Home,
    // redirect: "/customer",
    children:[
      {
         path: '/customer',
         name: '客户信息',
         component: Customer,
        //  children:[{
        //     path: '/customer/add',
        //     name: '添加用户',
        //     component: AddCustomer,
        //  }
        //  ]
      },
      {
         path: '/personaltrainer',
         name: '教练信息',
         component: Personaltrainer
      },
     {
        path: '/levelprice',
        name: '训练级别费用',
        component: LevelPrice
     },
     {
      path: '/customertraininginfo',
      name: '客户训练情况',
      component: CustomerTrainingInfo
     },
    ]
  },
  

  // {
  //   path: '/about',
  //   name: 'About',
  //   // route level code-splitting
  //   // this generates a separate chunk (about.[hash].js) for this route
  //   // which is lazy-loaded when the route is visited.
  //   component: function () {
  //     return import(/* webpackChunkName: "about" */ '../views/About.vue')
  //   }
  // }

]

const router = new VueRouter({
  mode: 'history',
  base: process.env.BASE_URL,
  routes
})

export default router
