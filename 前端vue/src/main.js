import Vue from 'vue'
import './plugins/axios'
import App from './App.vue'
import router from './router'
import store from './store'
import Home from './views/Home.vue'
import './plugins/element.js'

Vue.config.productionTip = false

new Vue({
  router,
  store,
  data: function(){
    return{
        preURL: 'http://120.78.10.173/',
    }
  },
  render: function (h) { return h(App) }
}).$mount('#app')
